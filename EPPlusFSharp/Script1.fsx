﻿#if INTERACTIVE
#r @"..\packages\EPPlus\lib\net40\EPPlus.dll"
#r @"..\packages\Casaubon\lib\Casaubon.dll"
#endif

open System
open System.IO
open OfficeOpenXml
open Casaubon.Core


let rootDir =  __SOURCE_DIRECTORY__ + "\..\data"
//Directory.Exists rootDir
let fName = "Book1.xlsx"
let xlsFile = Path.Combine([|rootDir;fName|])
//File.Exists xlsFile

let excelFile = FileInfo(xlsFile) //EPPlus 
let package = new ExcelPackage(excelFile)

package.Workbook.Worksheets.Count //2                    '

let  xls = ExcelFactory.make(xlsFile) //use some helpers from Causabon
let xls1 = worksheetByNumber 1 xls
let rows1 = rows xls1
let cells1 = cells xls1
let cols1 = cols xls1

cells1 |> Seq.map ( fun x -> x.GetValue<string>()) |> Seq.toList  // Retrieve cells as string
cells1 |> Seq.map (fun x -> x.Value) |> Seq.toList  //Retrieve cells as typed values
rows1 |> Seq.map (fun x -> x |> Seq.map (fun x -> x.Value)) |> Seq.toList //Rows as List of Lists
cols1 |> (Seq.map >> Seq.map) (fun x -> x.Value) |> Seq.toList  //Columns as List of Lists

